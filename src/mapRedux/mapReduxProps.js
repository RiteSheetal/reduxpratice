import React from 'react'
import { connect } from 'react-redux'
import { addPost, deletePost } from '../actions/postAction'

export const mapStatetoProps = (state) => {
    return {
        posts: state.posts
    }
}

export const mapaddPostToProps = (dispatch) => {
    return {
        addPost: (id, title) => { dispatch(addPost(id, title)) }
    }
}

export const mapdeletePostToProps = (dispatch) => {
    return {
        deletePost: (id) => { dispatch(deletePost(id)) }
    }
}
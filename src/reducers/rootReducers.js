const initState = {
  posts: [
    { id: '1', title: 'sheetal' },
    { id: '2', title: 'Shreya' },
    { id: '3', title: 'Disha' },

  ]
}

const RootReducers = (state = initState, action) => {
  if (action.type === 'DELETE_POST') {
    let newPosts = state.posts.filter(post => {
      return action.id !== post.id
    }

    );
    return {
      ...state,
      posts: newPosts
    }
  } else if (action.type === 'ADD_POST') {
    let newPosts = state.posts
    newPosts.push({ id: action.id, title: action.title })
    return {
      ...state,
      posts: newPosts
    }
  }
  return state;
}

export default RootReducers
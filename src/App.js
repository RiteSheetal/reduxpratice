
import React, { Component } from 'react';
import Navbar from './components/Navbar'
import { BrowserRouter, Route, Routes} from 'react-router-dom'
import Home from './components/Home';
import Contact from './components/Contact'
import About from './components/About'
import Post from './components/Post';




class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">

          <Navbar />
          <Routes>

            <Route path="/Home" element={<Home />} />
            <Route path="/Contact" element={<Contact />} />
            <Route path="/About" element={<About />} />
            {/* <Route path="Post" element={<Post />} />  */}
            <Route path="/:post_id" element={<Post />} />

          </Routes>

        </div>
      </BrowserRouter>
    );
  }
}

export default App;

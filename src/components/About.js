import React from 'react'
import Rainbow from '../Hoc/Rainbow'
import { useNavigate } from 'react-router-dom'

const About =() =>{
    const navigate = useNavigate();
    /* setTimeout(() => {
       
        { navigate('/Contact'); } 

    },5000) */
    return(
       <div className="container">
           <h4 className="center">About</h4>
           <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae aliquam dolorum modi, necessitatibus ut similique sint incidunt quia velit sapiente earum culpa labore beatae voluptates voluptatem? In, distinctio. Aut, fugiat.</p>
       </div>
    )
}
export default Rainbow(About)  
import React, { Component } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import postAction, { deletePost } from '../actions/postAction'
import {mapStatetoProps, mapdeletePostToProps} from '../mapRedux/mapReduxProps'
//  import axios from 'axios'

import { connect } from 'react-redux'
class Post extends Component {
    /*  state ={
         post:null
      
    }
    componentDidMount(){
        let id=this.props.params.post_id;
       this.setState({
            id:id
        }) 
        axios.get('https://jsonplaceholder.typicode.com/posts/' + id)
        .then(res => {
            this.setState({
            })
           console.log(res)
        })  
    }  */
    handleClick = () => {
        this.props.deletePost(this.props.posts.id);
        this.props.navigate('/Home');

    }
    render() {
        console.log(this.props)

        const post = this.props.posts ? (
            <div className="post">
                <h4 className="center">{this.props.posts.title}</h4>
                <p>{this.props.posts.body}</p>
                <div className='center'>
                    <button className='btn grey' onClick={this.handleClick}>
                        Delete Post
                    </button>
                </div>
            </div>

        ) : (
            <div className="center">Loading post...</div>
        )
        console.log('this.props', this.props)
        return (
            <div className="container">
                <h4 className="center">{post}</h4>
            </div>
        )
    }
}

function WithNavigate(props) {
    let navigate = useNavigate()
    let param = useParams()
    console.log('withnavigate : param', param)
    console.log('withnavigate : props.posts', props.posts)
    return <Post {...props} navigate={navigate} params={param} posts={props.posts.find(post => post.id === param.post_id)} />
}


/* const mapStatetoProps = (state) => {
    return {
        posts: state.posts
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deletePost: (id) => { dispatch(deletePost(id)) }
    }
} */

export default connect(mapStatetoProps, mapdeletePostToProps)(WithNavigate)
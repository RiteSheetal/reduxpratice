import React, { Component } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import PokeBall from '../components/PokeBall.png'
import { useNavigate } from 'react-router-dom'
import { connect } from 'react-redux'
import { addPost } from '../actions/postAction'
import { Button } from '@mui/material'
// import mapReduxProps from '../Hoc/mapReduxProps'
import { mapStatetoProps, mapaddPostToProps } from '../mapRedux/mapReduxProps'
class Home extends Component {

    state = {
        posts: []

    }

    componentDidMount() {
        //    axios.get('https://jsonplaceholder.typicode.com/posts')
        //    .then(res =>{
        //        console.log(res)
        //        this.setState({
        //            posts:res.data.slice(0,10)
        //        })
        //    })
        this.setState({ posts: this.props.posts })
    }

    componentWillReceiveProps(newProps) {
        const posts = newProps.posts
        this.setState({ posts })
    }

    addPost = () => {
        alert('addPost called')
        this.props.addPost('4', 'Shrushti')
    }

    render() {
        console.log('this.props', this.props)
        // const { posts } = this.props;
        console.log('this.state.posts', this.state.posts)
        // const postList = this.state.posts.length ? (
        //     this.state.posts.map(post => {
        //         return (
        //             <div className="post card" key={post.id}>
        //                 <img src={PokeBall} alt='A PokeBall' />
        //                 <div className="card-content">
        //                     <Link to={'/' + post.id}>
        //                         <span className="card-title red-text">{post.title}</span>
        //                     </Link>
        //                     <p>{post.body}</p>
        //                 </div>
        //             </div>
        //         )
        //     })
        // ) : (
        //     <div className="center">
        //         No post yet
        //     </div>

        // )
        return (
            <div className="container home">
                <h4 className="center">Home</h4>
                <Button onClick={this.addPost}>Add Post</Button>
                {this.state.posts.length ? (
                    this.state.posts.map(post => {
                        return (
                            <div className="post card" key={post.id}>
                                <img src={PokeBall} alt='A PokeBall' />
                                <div className="card-content">
                                    <Link to={'/' + post.id}>
                                        <span className="card-title red-text">{post.title}</span>
                                    </Link>
                                    <p>{post.body}</p>
                                </div>
                            </div>
                        )
                    })
                ) : (
                    <div className="center">
                        No post yet
                    </div>
                )}
            </div>
        )
    }
}


function WithNavigate(props) {
    let navigate = useNavigate()
    return <Home {...props} navigate={navigate} />
}

// export default connect(mapStatetoProps, mapDispatchToProps)(WithNavigate)
export default connect(mapStatetoProps, mapaddPostToProps)(WithNavigate)